
-- INSERT INTO transactions (date, expense_type, amount, account_id) VALUES ('2020-03-28','invoice',999,'4d9fd361-8b52-4050-8ad9-dff1fe61d5ed');
-- select *, count(*) from transactions group by date, expense_type, amount, amount HAVING count(*)>1;

-- Given an account id and date range, find the sum and count of transactions for each expense type

SELECT expense_type FROM transactions WHERE account_id = '4d9fd361-8b52-4050-8ad9-dff1fe61d5ed' AND date BETWEEN '2020-03-25' AND '2020-03-28';


SELECT expense_type, count(*), sum(amount)
FROM transactions 
WHERE account_id = '4d9fd361-8b52-4050-8ad9-dff1fe61d5ed' 
AND date BETWEEN '2020-03-25' AND '2020-03-28' 
GROUP BY expense_type;

SELECT expense_type, count(*), sum(amount)
FROM transactions 
WHERE account_id ='4d9fd361-8b52-4050-8ad9-dff1fe61d5ed'
GROUP BY expense_type;