# Prodsight Backend Excercise

### Python API

The [CSV file](https://prodsight-public.s3-eu-west-1.amazonaws.com/backend-exercise-data.csv) contains 
fictitious transaction records for 100 accounts. There are a total of 730,500 records with this structure

Name | Format | Description
--- | --- | ---
id | UUID | Unique Transaction ID
date | YYYY-MM-DD | Transaction Date
expense_type | invoice or salary or services or office-supplies or travel |
account_id | UUID | Account ID
amount | float | Transaction amount

#### Create

<details>
    <summary>✅ Given an expense_type, amount, date and account_id a new transaction is created 
    and a unique transction ID is returned</summary>

##### Request
`POST localhost:8000/transactions`

```json
{
    "account_id": "387ee6b9-520d-4c51-a9e4-6eb2ef15887d",
    "date": "2020-03-24",
    "amount": 20.0,
    "expense_type": "travel"
}
```

###### curl
```bash
curl -H "Content-Type: application/json" \
  -X POST \
  -d @transaction.json \
  http://localhost:8000/transactions
``` 

##### Response

```json
{ "id": "567ee6b9-520d-7r51-a9e4-6eb3ef15887d"}
```

</details>

#### Delete

<details>
    <summary>✅ Given a unique transaction ID, the corresponding record should be deleted</summary>

##### Request
```bash
curl -H "Content-Type: application/json" \
  -X DELETE \
  http://localhost:8000/transactions/a5157d22-1d37-428c-83b7-f63363c6f43c
```

##### Response
`Status 204 No Content`

</details>

#### Get account breakdown
<details>
    <summary>✅ Given an account id and date range, find the sum and count of transactions for each expense type</summary>

##### Request
`GET localhost:8000/456ed2b9-520d-4c51-a9e4-6gb1ef15887d/transactions?start_date=2020-03-01&end_date=2020-03-29`

###### curl
```bash
curl -H "Content-Type: application/json" \
  -X GET \
  http://localhost:8000/456ed2b9-520d-4c51-a9e4-6gb1ef15887d/transactions?start_date=2000-03-01&end_date=2001-03-29
``` 

##### Response
```json
{
    "start_date": "2020-03-01",
    "end_date": "2020-03-24",
    "transactions": {
        "invoice": { "sum": 200.0, "count": 10},
        "salary": { "sum": 600.0, "count": 10},
        "services": { "sum": 600.0, "count": 20},
        "office-supplies": { "sum": 600.0, "count": 10},
        "travel": { "sum": 600.0, "count": 10}
    }

}
```
</details>

<details>
    <summary>✅ If no date range is provided, the endpoint should return a breakdown for the entire available period</summary>

##### Request
`POST localhost:5000/{account_id}/transactions`

##### Response
```json
{
    "start_date": "2020-03-01",
    "end_date": "2020-03-24",
    "transactions": [
        {"invoice": { "sum": 200.0, "count": 10} },
        {"salary": { "sum": 600.0, "count": 10} },
        {"services": { "sum": 600.0, "count": 10} },
        {"office-supplies": { "sum": 600.0, "count": 10} },
        {"travel": { "sum": 600.0, "count": 10} }
    ]

}
```
</details>


### Instructions
- Set up public repo on Github
- Independently resolve missing/unclear requirements
- Write this application so that it can be easily set up and torn down on new machines (Docker)
- Use best software engineering practices
- Resolve any missing or ambiguous requirements yourself and outline your decisions
- Timer (4hs)
- Share before Friday

### Questions
- Why use FastAPI?
    * I wanted to learn something new. People at work had mentioned it as the modern Python web framework.
    * I hadn't worked extensively on Django or Flask over the last 2 years.
    * It's extremenly peformant.
    * Compared to Django, it's not as tightly coupled to RDBs like MySQL and Postgres. And it was designed with APIs in mind.
    * Compared to Flask, it's inspired by it's simplicity, but some of the components that come as third-party plugins are built in.
    * Path operations are super intuitive `@app.get()` just like `requests.get()`
    * Auto-generated OpenAPI docs (this meant I didn't even have to curl the endpoint but just use the Swagger UI)
    * Uses type hints for serialisation and validation (no need to `from marshmallow import fields;...fields.Str()`)

### Tasks
- [ ] Copy contents of CSV into Postgres
    - [X] Find COPY command (`\copy [Table/Query] to '[Relative Path/filename.csv]' csv header`)
    - [X] Set up Postgres docker image
    - [X] `psql -h localhost -U postgres -a -f populate.sql`
    - [X] Run command `docker-compose -f stack.yml up -d`
- [X] Create db index for `date` column
- [X] Add [`UNIQUE` constraint](https://www.postgresqltutorial.com/postgresql-unique-constraint/)
- [ ] Add `NOT NULL` constraint `date`
- [ ] Use ISO8601 date format and query params to pass date range
- [ ] User auth
- [ ] Monitoring - metrics, alerts, logs
- [X] Open API docs
- [ ] Tests
- [ ] Pagination - cusor based
- [ ] Error Handling 
- [ ] Validation
- [ ] Authentication - token based

- [ ] Design the API

- [ ] Decide between FastAPI and Flask
- [ ] Poetry or regular requirements.txt

### Things to cover

- Assumptions
    - What are the assumptions to you need to make?
        * Token based authentication
    - Do we need to clarify anything?
    - What are the main challenges
        * Make sure we can scale the reads?
        * What will the usage patterns look like? Read/Write on the DB
- State to store
    - What state do you need to store?
        * Envvars for the DB host, user, pass, port (stored in kube secrets, or vault, Mozzilla SOPs)
        * Store transactions in DB
            * index on date because we will perform range queries
            * provide a contraint on (date, expense_type, account_id, amount)
- Software components
    - What general components do you need?
        * N-tier architectuer
        * Web app
            * Main app with routes and path operations
            * Repository abstraction which will reduce boilerplate and make code more testable
            * Schemas - for data validation
            * Models - for the ORM
        * DB
            * Relational DB works well here
- Errors
    - What errors could occur? How does the system respond to them
        * Data validation errors - handle with 400s
        * Other 500 errors - bugs, poorly written code; use Sentry or Rollbar
        * DB could be down - alerting, maybe use a managed service
        * Network/DNS issues - retries
        * Too many requests
            * Load balancing across hosts
            * Rate limiting
                * X-RateLimit-Limit: 5000
                * X-RateLimit-Remaining: 4999
                * X-RateLimit-Reset: 1372700873

- Monitoring
    - What are the key things to monitor
        * USE - Utilisation; Saturation; Errors
        * RED - Rate; Errors; Duration
        * Request throughput (requests per second)
        * Request latency (Duration)
        * Saturation (how many concurrent requests)
        * Error rate
- Scaling
    - What are the bottlenecks?
        * Depends on the usage, no point in premature optimisation
    - What will happen if we had more data?
        * We need to introduce cursor based pagination instead of offset
        * We should do DB replication to ensure there is no data loss
        * We could shard the DB by account_id (vertical) or by table (horizontal)
- Concurrency
    - Do we want this system to be concurrent?
        * https://medium.com/building-the-system/gunicorn-3-means-of-concurrency-efbb547674b7
        * The workloads will be I/O bound
        * Yes we should serve multiple requests
        * FastAPI can be async but not for db calls `uvloop` under the hood - drop in replacement of asyncio event loop
        * There is a Docker image with optimisations using `gunicorn`

        * """In Python, threads and pseudo-threads are a means of concurrency, but not parallelism; while workers are a means of both concurrency and parallelism."""
- Deployment
    - What specific software would you use?
        * Docker on Docker Hub or Quay or AWS ECR
        * Kubernetes cluster - managed on AWS or Google Cloud
        * How would you deploy the system?
            * In an ideal world we would have a CI/CD Pipeline
            `test and build image -> auto deploy to staging -> manual deploy to prod (by pressing a button in the pipeline)`
- Availability
    - What would happen if a dependency is unavailable?
        * Use pgbouncer to switch to replica
        * If running in Kubernetes there should be multiple pods (replicas running)
- Testing
    - What are the most important parts to test?
        * There is a A/C spec so we could start with an end to end test using BDD
        * Use the repository to test the data access and that it's returning the correct data (mock the SQL alchemy session)
        * Testing path operations can be performed using the built in test client for FastAPI
    - How would you implement this?
- Security
    - Token based authentication (api-key = account_id or hash)
    - OAuth1,2 are available along wit
- Code review
    - How would you review the code?
    - What are some improvements you could make to the code structure?
    - What next steps would you take if you were working on this system for real?

volumes:
  prodsight-data:
    driver: local

networks:
  prodsight-env:
    external: true

### Tech

- https://github.com/zalando/connexion
- Flask
- FastAPI
- poetry
- pre-commit (https://pre-commit.com/)

### Questions

- What is the use of `async`?
- Why did you use FastAPI (Pros & Cons)
    - Very simple to use
    - Validation and OpenAPI docs for free
    - Modern - Python 3.6+
- How would you deploy this?
- What's .DEFAULT_TARGET in the Makefile
    - The first thing that runs if no other target is specified
- What's .PHONY in the Makefile
    - Make assumes that it's targeting files on the file system. This allows you to have fake target hence the name phony.
