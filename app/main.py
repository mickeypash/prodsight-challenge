from typing import List
from datetime import date
from uuid import UUID

import uvicorn
from fastapi import Depends, FastAPI, HTTPException
from starlette import status
from starlette.responses import Response
from starlette_prometheus import metrics, PrometheusMiddleware
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

# We need to have an independent database session/connection (SessionLocal) per request,
# use the same session through all the request and then close it after the request is finished.
# Our dependency will create a new SQLAlchemy SessionLocal that will be used in a single request,
# and then close it once the request is finished.
def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


app = FastAPI(
    title="Prodsight Backend Excercise", description="Homework Exercise for a Backend Engineer", version="0.1.0",
)

# Adds Prometheus metrics using Starlette middleware
app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics/", metrics)


@app.post("/transactions")
def create_transation(transaction: schemas.TransactionCreate, db: Session = Depends(get_db)):

    # TODO add a DB contraint and check if the transaction exists
    # transaction = crud.get_transaction(db, transaction)
    # if transaction:
    #     raise HTTPException(status_code=400, detail="Transaction already added")
    transaction = crud.create_transaction(db=db, transaction=transaction)
    return {"id": transaction.id}


@app.get("/transactions", response_model=List[schemas.TransactionBase])
def read_transactions(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    transactions = crud.get_transactions(db, skip=skip, limit=limit)
    return transactions


@app.get("/{account_id}/transactions")
def read_transaction(
    account_id: UUID, start_date: date = None, end_date: date = None, db: Session = Depends(get_db),
):

    account_summary = {
        "start_date": start_date,
        "end_date": end_date,
        "transactions": {},
    }

    results = crud.get_account_summary(db, account_id=account_id, start_date=start_date, end_date=end_date)

    for row in results:
        expense_type, count, total_sum = row
        account_summary["transactions"][expense_type] = {
            "count": count,
            "sum": float(total_sum),
        }

    return account_summary


@app.delete(
    "/transactions/{transaction_id}", status_code=status.HTTP_204_NO_CONTENT, response_class=Response,
)
def delete_transaction(transaction_id: UUID, db: Session = Depends(get_db)):
    crud.delete_transaction(db, transaction_id=transaction_id)
    return ""


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
