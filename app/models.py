from datetime import date

from sqlalchemy import Column, String
from sqlalchemy.sql.expression import text
from sqlalchemy.dialects.postgresql import UUID, DATE, NUMERIC

from .database import Base


class Transaction(Base):
    __tablename__ = "transactions"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, server_default=text("uuid_generate_v4()"),)
    account_id = Column(UUID(as_uuid=True), nullable=False)  # UUID
    date = Column(DATE, index=True, nullable=False)
    expense_type = Column(String)
    amount = Column(NUMERIC)  # Transaction amount
