from datetime import date
from uuid import UUID

from pydantic import BaseModel
from enum import Enum

# To avoid confusion between the SQLAlchemy models and the Pydantic models,
# we will have the file models.py with the SQLAlchemy models,
# and the file schemas.py with the Pydantic models.


class ExpenseType(str, Enum):
    invoice = "invoice"
    salary = "salary"
    services = "services"
    office_supplies = "office-supplies"
    travel = "travel"


class TransactionBase(BaseModel):
    id: UUID  # UUID
    account_id: UUID  # UUID
    date: date
    expense_type: ExpenseType
    amount: float  # Transaction amount

    class Config:
        orm_mode = True


class TransactionCreate(BaseModel):
    account_id: str  # UUID
    date: date
    expense_type: ExpenseType
    amount: float  # Transaction amount
