from uuid import UUID
from datetime import date

from sqlalchemy.orm import Session
from . import models, schemas

# By creating functions that are only dedicated to interacting with the database
# (get a user or an item) independent of your path operation function,
# you can more easily reuse them in multiple parts and also add unit tests for them.

account_summary_query = """
SELECT expense_type, count(*), sum(amount)
FROM transactions 
WHERE account_id =:account_id
AND date BETWEEN :start_date AND :end_date
GROUP BY expense_type;
"""

account_summary_query_all = """
SELECT expense_type, count(*), sum(amount)
FROM transactions 
WHERE account_id =:account_id
GROUP BY expense_type;
"""


def get_transaction_by_id(db: Session, transaction_id: int):

    return db.query(models.Transaction).filter(models.Transaction.id == transaction_id).first()


def get_account_summary(db: Session, account_id: UUID, start_date: date = None, end_date: date = None):

    if start_date and end_date:
        params = {
            "account_id": account_id,
            "start_date": start_date,
            "end_date": end_date,
        }
        return db.execute(account_summary_query, params)

    return db.execute(account_summary_query_all, {"account_id": account_id})


def get_transactions(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Transaction).offset(skip).limit(limit).all()


def create_transaction(db: Session, transaction: schemas.TransactionCreate):
    db_item = models.Transaction(**transaction.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def delete_transaction(db: Session, transaction_id: str):
    db.query(models.Transaction).filter(models.Transaction.id == transaction_id).delete()
    db.commit()
