-- We need this extension to generate UUIDs
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Create table
DROP TABLE IF EXISTS transactions;
CREATE TABLE transactions(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    date DATE,
    expense_type VARCHAR(16),
    amount NUMERIC,
    account_id uuid

);

-- Add an index for the date column
CREATE INDEX ON transactions (date);

-- Add Unique contraint
ALTER TABLE transactions
ADD CONSTRAINT transactions_idx UNIQUE(date, expense_type, amount, account_id);

-- Copy files over from CSV
\copy transactions FROM 'backend-exercise-data.csv' DELIMITER ',' CSV HEADER;
