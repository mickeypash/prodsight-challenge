from fastapi import Depends
import pytest

from app import models, crud
from app.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

# use conftest or pytest fixtures to remove the session from here
@pytest.fixture(scope="function")
def db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def test_get_transaction_by_id(db):

    transaction_id = "8c556e5c-e3d1-4068-93ab-48db082bd408"
    test = crud.get_transaction_by_id(db, transaction_id=transaction_id)
    assert test.id == transaction_id


def test_get_account_summary(db):

    account_id = "4d9fd361-8b52-4050-8ad9-dff1fe61d5ed"
    start_date = "2000-01-01"
    end_date = "2001-01-01"

    account_summary = {
        "start_date": start_date,
        "end_date": end_date,
        "transactions": {},
    }

    my_dict = {}

    results = crud.get_account_summary(db, account_id=account_id, start_date=start_date, end_date=end_date)

    for row in results:
        expense_type, count, total_sum = row
        account_summary["transactions"][expense_type] = {
            "count": count,
            "sum": float(total_sum),
        }

    print(account_summary)
    breakpoint()
