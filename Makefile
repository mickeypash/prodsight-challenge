.DEFAULT_GOAL: help
.PHONY: help install run format lint test docker push kube_yaml generate_requirements dep-dev

help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

bootstrap: ## Bootstrap env
	pip install --upgrade -q poetry==1.0.2

install: ## Install project dependencies
	poetry install

run: # Run app locally
	poetry run uvicorn --host 0.0.0.0 app.main:app --reload

gen_reqs: # Generate requirements.txt
	poetry export -f requirements.txt -o requirements.txt

format: # Format code
	poetry run black -l 120 ./**/*.py

test: ## Run tests
	poetry run pytest
